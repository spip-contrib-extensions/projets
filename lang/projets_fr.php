<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'cadres_aide' => "On peut caractériser un projet selon le cadre dans lequel il a été entrepris.

Par exemple, tel projet pourrait être entrepris dans le cadre :
- d'une prestation forfaitaire,
- d'un devis,
- en régie,  ou encore
- dans un cadre purement personnel

Les cadres de projet évitent l'utilisation des mots clé pour caractériser les projets.

Vous pouvez créer autant de cadres de projet que nécessaire.",

	'cfg_exemple' => 'Exemple',
	'cfg_exemple_explication' => 'Explication de cet exemple',
	'cfg_titre_parametrages' => 'Paramétrages',

	// E
	'explication_cfg_objets' => 'Veuillez sélectionner les objets éditoriaux auquels vous désirez lier des projets.',

	// L
	'label_cfg_objets' => 'Liste des objets éditoriaux&nbsp;:',

	// P
	'projets_titre' => 'Projets',
	'projets_aide' => "Cette page liste tous les projets gérés dans ce site.
Il est possible de filtrer les projets selon leur statut dans la boite ci-dessus, et de les trier selon les champs indiqués en couleur dans le tableau ci-contre.
[Voir la doc en ligne->@lien@].",

	// T
	'titre_page_configurer_projets' => 'Configurer le plugin Projets',

);

