<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_projets_cadre' => 'Ajouter ce cadre de projet',

	// C
	'champ_descriptif_explication' => 'Vous pouvez décrire plus précisément ce cadre de projet',
	'champ_descriptif_label' => 'Descriptif',
	'champ_titre_explication' => 'Donnez le nom de ce cadre de projet',
	'confirmer_supprimer_projets_cadre' => 'Êtes-vous sûr de vouloir supprimer les informations relatives à ce cadre de projets&nbsp;?',

	//E
	'explication_supprimer_projets_cadre' => 'La suppression de ce cadre de projets risque de rompre les liens entres objets.',

	// I
	'icone_creer_projets_cadre' => 'Créer un cadre de projet',
	'icone_modifier_projets_cadre' => 'Modifier ce cadre de projet',
	'info_1_projets_cadre' => 'Un cadre de projet',
	'info_aucun_projets_cadre' => 'Aucun cadre de projet',
	'info_nb_projets_cadres' => '@nb@ cadres de projet',
	'info_projets_cadres_auteur' => 'Les cadres de projet de cet auteur',

	// L
	'label_id_projets_cadre' => 'Identifiant du cadre de projet',
	'label_maj' => 'Date de mise à jour',
	'label_titre' => 'Titre',

	// R
	'retirer_lien_projets_cadre' => 'Retirer ce cadre de projet',
	'retirer_tous_liens_projets_cadres' => 'Retirer tous les cadres de projet',

	// S
	'supprimer_projets_cadre' => 'Supprimer ce cadre de projet',

	// T
	'texte_ajouter_projets_cadre' => 'Ajouter un cadre de projet',
	'texte_changer_statut_projets_cadre' => 'Ce cadre de projet est :',
	'texte_creer_associer_projets_cadre' => 'Créer et associer un cadre de projet',
	'titre_langue_projets_cadre' => 'Langue de ce cadre de projet',
	'titre_logo_projets_cadre' => 'Logo de ce cadre de projet',
	'titre_projets_cadre' => 'Cadre de projet',
	'titre_projets_cadres' => 'Cadres de projet',
	'titre_projets_cadres_rubrique' => 'Cadres de projet de la rubrique',
);

